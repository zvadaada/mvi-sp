import sys
import os

import argparse
import os
from satellite_enhancer.trainer import Trainer
from satellite_enhancer.dataset.satellite_dataset import SatelliteDataset
from satellite_enhancer.model.generator import Generator
from satellite_enhancer.model.discriminator import Discriminator
import matplotlib.pyplot as plt

import tensorflow as tf
tf.executing_eagerly()


def dataset():

    def dir_path(string):
        if os.path.isdir(string):
            return string
        else:
            raise NotADirectoryError(string)

    parser = argparse.ArgumentParser(description='Train Satellite super resolution')
    parser.add_argument('--dataset-path', dest="dataset_path", type=dir_path,
                        help='Path to satellite dataset', required=True)

    arguments = parser.parse_args()
    print(arguments.dataset_path)

    satellite_train = SatelliteDataset(scale=4, images_dir=arguments.dataset_path)
    train_ds, test_ds = satellite_train.dataset(batch_size=16, random_transform=True)

    return train_ds, test_ds


def train():
    train_ds, test_ds = dataset()

    trainer = Trainer(Generator(), Discriminator())
    trainer.train(train_ds, test_ds)


def predict():
    train_ds, test_ds = dataset()
    trainer = Trainer(Generator(), Discriminator())

    # TODO: how to retrive numpy data from tf.Dataset? Workaround with ugly for-loop
    lr = None
    hr = None
    for _lr, _hr in train_ds.take(1):
        lr = _lr
        hr = _hr

    for _sr, _hr in zip(trainer.predict(lr), hr):
        plt.imshow(_sr)
        plt.show()
        plt.imshow(_hr)
        plt.show()


if __name__ == '__main__':
    train()
    #predict()

