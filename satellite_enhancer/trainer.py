import os
print(os.listdir('./../'))
import sys
sys.path.append('./../')

import argparse
import math
import tensorflow as tf
import numpy as np
import os
from satellite_enhancer.model.generator import Generator
from satellite_enhancer.model.discriminator import Discriminator
import matplotlib.pyplot as plt


class Trainer(object):

    def __init__(self, generator: Generator, discriminator: Discriminator):

        self.generator = generator
        self.generator.load_weights('../trained_model/gen')

        self.discriminator = discriminator
        self.discriminator.load_weights('../trained_model/gen')

        # discriminator should learn then generator
        self.generator_optimizer = tf.keras.optimizers.Adam(lr=1e-5)
        self.discriminator_optimizer = tf.keras.optimizers.Adam(lr=1e-7)

        self.summary_writer_train = tf.summary.create_file_writer(os.path.join('summaries', 'train'))
        self.summary_writer_test = tf.summary.create_file_writer(os.path.join('summaries', 'test'))

    def predict(self, lr):

        lr = self.normalize(lr)
        sr = self.generator(lr, training=False)
        sr = self.denormalize(sr)

        return sr

    def train(self, train_dataset: tf.data.Dataset, test_dataset: tf.data.Dataset, num_epochs=100):

        for epoch in range(num_epochs):
            print(f'EPOCHE: {epoch}')

            num_setps = 0

            # 30 is (train_size / batch_size) TODO: fix ugly hotfix
            for lr, hr in train_dataset.take(30):
                num_setps += 1

                p_l, d_l = self.train_step(lr, hr)
                print(f'TRAIN {num_setps}/{epoch} perceptual loss = {p_l}, discriminator loss = {d_l}')

                with self.summary_writer_train.as_default():
                    tf.summary.scalar('perceptual_loss', p_l, step=self.generator_optimizer.iterations)
                    tf.summary.scalar('discriminator_loss', d_l, step=self.discriminator_optimizer.iterations)

            # run test step of size 1
            test_pl, test_dl = None, None
            for lr, hr, in test_dataset.shuffle(buffer_size=100).take(1):
                test_pl, test_dl = self.test_step(lr, hr)
            print(f'TEST {epoch}, perceptual loss = {test_pl:.3f}, discriminator loss = {test_dl:.3f}')

            with self.summary_writer_test.as_default():
                tf.summary.scalar('perceptual_loss', test_pl, step=self.generator_optimizer.iterations)
                tf.summary.scalar('discriminator_loss', test_dl, step=self.discriminator_optimizer.iterations)

            self.generator.save_weights('gen', save_format='tf')
            self.discriminator.save_weights('disc', save_format='tf')

    def train_step(self, lr, hr):

        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:

            lr = self.normalize(lr)
            hr = self.normalize(hr)

            sr = self.generator(lr, training=True)

            # plt.imshow(self.denormalize(hr[0]))
            # plt.show()
            # plt.imshow(self.denormalize(lr[0]))
            # plt.show()
            # plt.imshow(self.denormalize(sr[0]))
            # plt.show()

            hr_output = self.discriminator(hr, training=True)
            sr_output = self.discriminator(sr, training=True)

            print(f'HR_disc: {hr_output.numpy()}')
            print(f'SR_disc: {sr_output.numpy()}')

            perc_loss = self.generator.loss(hr, sr, sr_output)
            disc_loss = self.discriminator.loss(hr_output, sr_output)

            gradients_of_generator = gen_tape.gradient(perc_loss, self.generator.trainable_variables)
            gradients_of_discriminator = disc_tape.gradient(disc_loss, self.discriminator.trainable_variables)

            self.generator_optimizer.apply_gradients(
                zip(gradients_of_generator, self.generator.trainable_variables)
            )

            self.discriminator_optimizer.apply_gradients(
                zip(gradients_of_discriminator, self.discriminator.trainable_variables)
            )

        return perc_loss, disc_loss

    def test_step(self, lr, hr):

       lr = self.normalize(lr)
       hr = self.normalize(hr)

       sr = self.generator(lr, training=False)
       plt.imshow(self.denormalize(sr[0]))
       plt.show()

       hr_output = self.discriminator(hr, training=False)
       sr_output = self.discriminator(sr, training=False)

       perc_loss = self.generator.loss(hr, sr, sr_output)
       disc_loss = self.discriminator.loss(hr_output, sr_output)

       return perc_loss, disc_loss

    def flip_labels(self, label, prob=0.05):
        pass

    def normalize(self, img):
        """Normalize image to [-1,1]"""

        img = tf.cast(img, tf.float32)/127.5 - tf.ones_like(img, dtype=np.float32)
        #n_img = np.divide(img.astype(np.float32), 127.5) - np.ones_like(img, dtype=np.float32)

        return img

    def denormalize(self, img):
        # return tf.divide(tf.add(img, 1), 2.)
        return tf.cast(tf.multiply(tf.add(img, 1), 127.5), dtype=tf.uint32)

    def lr_exp_decay(self, epoch, lr):
        k = 0.1
        return self.init_lr * math.exp(-k * epoch)


if __name__ == '__main__':

    from satellite_enhancer.dataset.divk2_dataset import DIV2K
    from satellite_enhancer.dataset.satellite_dataset import SatelliteDataset

    #div2k_train = DIV2K(scale=4, subset='train', downgrade='bicubic')
    satelitte_train = SatelliteDataset(scale=4, images_dir='.satellite/images')

    train_ds = satelitte_train.dataset(batch_size=16, random_transform=True)

    trainer = Trainer(Generator(), Discriminator())

    # for x in train_ds:
    #     for image in zip(x[0], x[1]):
    #         print(image[0].shape)
    #         print(image[1].shape)

    trainer.train(train_ds, num_epochs=500)
