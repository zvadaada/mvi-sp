import os
import tensorflow as tf

from typing import Tuple
from itertools import product
from tensorflow.python.data.experimental import AUTOTUNE


class SatelliteDataset:

    def __init__(self, scale=4, images_dir='./images'):

        self.scale = scale
        self.image_ids = range(0, 51)  # 50 images
        self.image_parts = range(0, 12)  # cut into 12 parts each

        self.images_dir = images_dir

        self.test_size = 0.2

        os.makedirs(images_dir, exist_ok=True)

    def __len__(self):
        return len(self.image_ids)

    def dataset(self, batch_size=16, repeat_count=None, random_transform=True) -> Tuple[tf.data.Dataset, tf.data.Dataset]:

        ds_train = tf.data.Dataset.zip((self.lr_train_dataset(), self.hr_train_dataset()))
        ds_test = tf.data.Dataset.zip((self.lr_test_dataset(), self.hr_test_dataset()))

        ds_train = self.prepare_dataset(ds_train, batch_size, repeat_count, random_transform)
        ds_test = self.prepare_dataset(ds_test, batch_size, repeat_count, random_transform)

        return ds_train, ds_test

    def prepare_dataset(self, ds: tf.data.Dataset, batch_size=16, repeat_count=None, random_transform=True) -> tf.data.Dataset:

        if random_transform:
            ds = ds.map(lambda lr, hr: random_crop(lr, hr, scale=self.scale), num_parallel_calls=AUTOTUNE)
            ds = ds.map(random_rotate, num_parallel_calls=AUTOTUNE)
            ds = ds.map(random_flip, num_parallel_calls=AUTOTUNE)
        ds = ds.batch(batch_size)
        ds = ds.repeat(repeat_count)
        ds = ds.prefetch(buffer_size=AUTOTUNE)

        return ds

    def hr_train_dataset(self):
        ds = self._images_dataset(self._hr_image_files(is_test=False))
        return ds

    def hr_test_dataset(self):
        ds = self._images_dataset(self._hr_image_files(is_test=True))
        return ds

    def lr_train_dataset(self):
        ds = self._images_dataset(self._lr_image_files(is_test=False))
        return ds

    def lr_test_dataset(self):
        ds = self._images_dataset(self._lr_image_files(is_test=True))
        return ds

    def _hr_image_files(self, is_test=False):
        images_dir = self._hr_images_dir()
        tile_names = os.listdir(images_dir)

        test_idx = int(len(tile_names) * (1 - self.test_size))
        if not is_test:
            tile_names = tile_names[0:test_idx]
        else:
            tile_names = tile_names[test_idx:]

        return [os.path.join(images_dir, tile_name) for tile_name in tile_names]

    def _lr_image_files(self, is_test=False):
        images_dir = self._lr_images_dir()
        tile_names = os.listdir(images_dir)

        test_idx = int(len(tile_names) * (1 - self.test_size))
        if not is_test:
            tile_names = tile_names[0:test_idx]
        else:
            tile_names = tile_names[test_idx:]

        return [os.path.join(images_dir, tile_name) for tile_name in tile_names]
        #return [os.path.join(images_dir, f'tile1-{image_id}.jpg') for (image_id, image_part) in product(self.image_ids, self.image_parts)]

    def _hr_images_dir(self):
        return os.path.join(self.images_dir, 'HR')

    def _lr_images_dir(self):
        return os.path.join(self.images_dir, 'LR')

    def split_dataset(self, dataset: tf.data.Dataset) -> Tuple[tf.data.Dataset]:
        train_ds, test_ds = dataset.even_splits('train', n=2)

        return train_ds, test_ds

    @staticmethod
    def _images_dataset(image_files):
        ds = tf.data.Dataset.from_tensor_slices(image_files)
        ds = ds.map(tf.io.read_file)
        ds = ds.map(lambda x: tf.image.decode_jpeg(x, channels=3), num_parallel_calls=AUTOTUNE)
        return ds

# -----------------------------------------------------------
#  Transformations
# -----------------------------------------------------------


def random_crop(lr_img, hr_img, hr_crop_size=96, scale=2):
    lr_crop_size = hr_crop_size // scale
    lr_img_shape = tf.shape(lr_img)[:2]

    lr_w = tf.random.uniform(shape=(), maxval=lr_img_shape[1] - lr_crop_size + 1, dtype=tf.int32)
    lr_h = tf.random.uniform(shape=(), maxval=lr_img_shape[0] - lr_crop_size + 1, dtype=tf.int32)

    hr_w = lr_w * scale
    hr_h = lr_h * scale

    lr_img_cropped = lr_img[lr_h:lr_h + lr_crop_size, lr_w:lr_w + lr_crop_size]
    hr_img_cropped = hr_img[hr_h:hr_h + hr_crop_size, hr_w:hr_w + hr_crop_size]

    return lr_img_cropped, hr_img_cropped


def random_flip(lr_img, hr_img):
    rn = tf.random.uniform(shape=(), maxval=1)
    return tf.cond(rn < 0.5,
                   lambda: (lr_img, hr_img),
                   lambda: (tf.image.flip_left_right(lr_img),
                            tf.image.flip_left_right(hr_img)))


def random_rotate(lr_img, hr_img):
    rn = tf.random.uniform(shape=(), maxval=4, dtype=tf.int32)
    return tf.image.rot90(lr_img, rn), tf.image.rot90(hr_img, rn)
