# Satellite Images Enhancer

Read more about the image enhencer in the provided report.pdf

# Dataset
The dataset can be download [from this link!](https://drive.google.com/drive/folders/1eR2bS--JNwXcEHXbjJwb9PiGKY0t6Ztv?usp=sharing)
and paste the dataset to the root of the project.

# Run training

The training process can be run via
```
pip install -r requirements.txt
python run.py --dataset-path ./images
```

# Run the prediction
```
python satellite_enhancer/converter/converter.py
```